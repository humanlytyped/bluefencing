import { map, traverseArray } from "fp-ts/lib/Either";
import { identity, pipe } from "fp-ts/lib/function";
import { NonEmptyArray } from "fp-ts/lib/NonEmptyArray";
import { eqGeoPoint, fromGeoJSON, GeoPoint } from "./geopoint";
import { last, uniq } from "fp-ts/lib/Array";
import { fold, Option } from "fp-ts/lib/Option";
import haversineDistance from "haversine-distance";

export type GeoFence = {
  radius: number;
  point: GeoPoint;
};

const defaultRadius = 250;

function unsafeGetOption<T>(optionT: Option<T>): T {
  return pipe(
    optionT,
    fold(() => {
      throw new Error("None Option received");
    }, identity)
  );
}

export function buildGeofence(
  points: readonly GeoPoint[],
  radius = defaultRadius
): GeoFence[] {
  points = uniq(eqGeoPoint)(points as GeoPoint[]);
  const minSpacing = 10;
  return points.reduce((fences: GeoFence[], point: GeoPoint): GeoFence[] => {
    if (fences.length > 0) {
      const lastFence = unsafeGetOption(last(fences));
      const distanceApart = haversineDistance(lastFence.point, point);
      const combinedRadii = lastFence.radius + radius;
      if (distanceApart < combinedRadii) {
        const excess = combinedRadii - distanceApart;
        const adjustedCombinedRadii = combinedRadii - excess;
        const r = Math.abs(adjustedCombinedRadii / 2) - minSpacing;
        lastFence.radius = r;
        return fences.concat({
          point,
          radius: r,
        });
      }
      return fences.concat({
        point,
        radius,
      });
    }
    return Array.of({
      point,
      radius,
    });
  }, []);
}

export function fromGeoJSONArray(gs: NonEmptyArray<[number, number]>) {
  return pipe(traverseArray(fromGeoJSON)(gs), map(buildGeofence));
}
