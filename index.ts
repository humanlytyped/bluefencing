import express, { RequestHandler } from "express";
import cors from "cors";
import helmet from "helmet";
import { defaultRuntime, QIO } from "@qio/core";
import { Client } from "@googlemaps/google-maps-services-js";
import { pipe } from "fp-ts/lib/function";
import { fromGeoJSONArray } from "./geofence";
import { fold } from "fp-ts/lib/Either";
import { match } from "fp-ts/lib/TaskEither";
import { geopointFromGoogleMaps } from "./geopoint";

const server = express()
  .use(cors())
  .use(helmet({ hidePoweredBy: true }))
  .use(express.json({ strict: true }));

type ResponseData =
  | { error: unknown; message: string; status: "error" }
  | { data: unknown; message: string; status: "success" };

function success(message: string, data: unknown): ResponseData {
  return { message, status: "success", data };
}

function failure(message: string, error: unknown): ResponseData {
  return { message, status: "error", error };
}

type ServerConfig = {
  port: number;
};

const readConfig = QIO.access((R: ServerConfig) => R.port);

interface MapsClientEnv {
  readonly client: Client;
}

const getClient = QIO.access((m: MapsClientEnv) => m.client);

const start = () =>
  readConfig.zip(getClient).map(([port, client]) => {
    server.post("/geofences/", handleCreateGeofence);
    server.get("/geopoint", handleGeopointFromMaps(client));

    return server.listen(port, "0.0.0.0");
  });

const handleCreateGeofence: RequestHandler = (req, res) => {
  const body = req.body.coordinates;
  return pipe(
    body,
    fromGeoJSONArray,
    fold(
      (error) => res.status(400).json(failure("Invalid request", error)),
      (geofence) => res.status(200).json(success("Geofence data", geofence))
    )
  );
};

const handleGeopointFromMaps = (c: Client): RequestHandler => async (
  req,
  res
) => {
  const { place_id, address } = req.query;
  const point = geopointFromGoogleMaps(c)({
    params: {
      key: "",
      address: address?.toString(),
      place_id: place_id?.toString(),
    },
  });
  await pipe(
    point,
    match(
      (error) => {
        switch (error.type) {
          case "network_error":
            res.status(500).json(failure("Network error", error.error.message));
            break;

          case "invalid_geopoint":
            res
              .status(203)
              .json(
                success(
                  "This geopoint looks invalid, but Google says that it's fine",
                  error.value
                )
              );
            break;

          case "maps_error_response":
            res.status(400).json(failure(error.status, error.message));
            break;
        }
      },
      (geopoint) =>
        res.status(200).json(success("Geocoding successful", geopoint))
    )
  )();
};

const program = start().provide({
  port: +(process.env.PORT ?? 3000),
  client: new Client(),
});

defaultRuntime().unsafeExecute(program);
