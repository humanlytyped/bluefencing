import { geopointFromGoogleMaps } from "./geopoint";
import axios, { AxiosResponse } from "axios";
import {
  Client,
  GeocodeResult,
  Status,
} from "@googlemaps/google-maps-services-js";
import { GeocodeResponseData } from "@googlemaps/google-maps-services-js/dist/geocode/geocode";
import { match, TaskEither } from "fp-ts/lib/TaskEither";
import { identity, pipe } from "fp-ts/lib/function";

const toPromise = <E, A>(te: TaskEither<E, A>): Promise<A> => {
  return pipe(
    te,
    match((error) => {
      throw error;
    }, identity)
  )();
};

describe("Creating Geopoints from Google Maps Geocoding API", () => {
  const expectedError = new Error("because I can");
  const erroringAxios = axios.create({
    adapter(_) {
      return Promise.reject(expectedError);
    },
  });

  const validGeopoint = { lng: -13.81, lat: 30.4 };

  const invalidGeopoint = { lng: 360, lat: 100 };

  const axiosStubReturningLocation = (location: any) =>
    axios.create({
      async adapter(config) {
        return {
          config,
          data: {
            results: [{ geometry: { location } } as GeocodeResult],
            status: Status.OK,
          },
        } as AxiosResponse<GeocodeResponseData>;
      },
    });

  const axiosReturningEmptyGeocodeResponse = axios.create({
    async adapter(config) {
      return {
        config,
        data: { status: Status.ZERO_RESULTS },
      } as AxiosResponse<GeocodeResponseData>;
    },
  });

  const geocodingRequest = {
    params: {
      address: "41 Turing Street, New Machines",
      key: "",
    },
  };

  test("creates a valid geopoint on successful response", async () => {
    const program = geopointFromGoogleMaps(
      new Client({
        axiosInstance: axiosStubReturningLocation(validGeopoint),
      })
    );
    const result = await toPromise(program(geocodingRequest));
    expect(result.latitude).toEqual(validGeopoint.lat);
    expect(result.longitude).toEqual(validGeopoint.lng);
  });

  test("returns invalid_geopoint on ZERO_RESULTS", async () => {
    const program = geopointFromGoogleMaps(
      new Client({
        axiosInstance: axiosReturningEmptyGeocodeResponse,
      })
    );

    try {
      await toPromise(program(geocodingRequest));
    } catch (error) {
      expect(error.type).toEqual("invalid_geopoint");
    }
  });

  test("returns invalid_geopoint if Geocoding returns an invalid geopoint", async () => {
    const program = geopointFromGoogleMaps(
      new Client({
        axiosInstance: axiosStubReturningLocation(invalidGeopoint),
      })
    );
    try {
      await toPromise(program(geocodingRequest));
    } catch (error) {
      expect(error.type).toEqual("invalid_geopoint");
      expect(error.value).toEqual(invalidGeopoint);
    }
  });

  test("returns the network error on network failure", async () => {
    const program = geopointFromGoogleMaps(
      new Client({ axiosInstance: erroringAxios })
    );
    try {
      await toPromise(program(geocodingRequest));
    } catch (error) {
      expect(error.type).toBe("network_error");
    }
  });
});
